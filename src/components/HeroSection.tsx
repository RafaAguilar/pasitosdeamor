import React from "react";
import "../styles/HeroSection.css";

export const HeroSection = () => {
  return (
    <div className="hero-section">
      <div className="container-text-area">
        <div className="info-text-area">
          <h1 className="title">HERE IS THE TITLE</h1>
          <p className="subtittle">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Nemo cumque
            ipsum debitis facilis illum nulla ducimus reiciendis, dolorum
            
          </p>
          <button className="button-24">
            CONTACT ME
          </button>
        </div>
      </div>
    </div>
  );
};
