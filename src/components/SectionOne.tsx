import React from "react";
import { GoTriangleRight, GoTriangleLeft } from "react-icons/go";
import {GiBrain} from "react-icons/gi"
import "../styles/SectionOne.css";
import { IconCard } from "./IconCard";

export const SectionOne = () => {
  return (
    <div className="container">
      <div className="main-section">
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            alignSelf: "center",
            width: "auto",
            justifyContent:"center"
          }}
        >
          <GoTriangleRight size={40} style={{ alignSelf: "center" }} />
          <h1 className="title">This section come a title</h1>
          <GoTriangleLeft size={40} style={{ alignSelf: "center" }} />
        </div>
        <div style={{display:"flex", flexDirection:"row", flexWrap:"wrap", alignSelf:"center", justifyContent:"center"}}>
        {[1,2,3,4].map((card,index) => <IconCard icon={<GiBrain size={100} style={{ alignSelf: "center" }} />}/>)}

        </div>
      </div>
    </div>
  );
};
