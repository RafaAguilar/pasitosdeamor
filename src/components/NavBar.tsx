import React from 'react';
import "../styles/NavBar.css";
import logo from "../assets/logo_1.png"
import {AiOutlineTwitter,AiOutlineFacebook,AiOutlineInstagram,AiOutlineLinkedin} from "react-icons/ai"


export const NavBar = () => {
  const links =  ["Lnk1","Lnk1","Lnk1","Nombre de link muy largo"]
  return <div className='nav-container'>
    <div className="navigation">
      <div className="social-media">
        <AiOutlineTwitter size={40} style={{margin:"0 5px"}}/>
        <AiOutlineFacebook size={40} style={{margin:"0 5px"}}/>
        <AiOutlineInstagram size={40} style={{margin:"0 5px"}}/>
        <AiOutlineLinkedin size={40} style={{margin:"0 5px"}}/>
      </div>
      <div className="nav-item">
        <img className="logo" src={logo}/>
          
        
        <div className="nav-links">
          {links.map((link,index) => 
            <li className="link">
                {link}
            </li>
          )}

        </div>

      </div>

    </div>

  </div>;
};
