import React from 'react';
import { FaTwitterSquare, FaFacebookSquare,FaInstagramSquare,FaLinkedin } from "react-icons/fa";
import "../styles/Header.css"

export const Header = () => {
  return <div className='main-container'>
      <div className='child-container'>
          <div className='social-media-container'>
              <FaTwitterSquare style={{fontSize:"40px", color:"white", margin:"0 3px"}}/>
              <FaFacebookSquare style={{fontSize:"40px", color:"white", margin:"0 3px"}}/>
              <FaInstagramSquare style={{fontSize:"40px", color:"white", margin:"0 3px"}}/>
              <FaLinkedin style={{fontSize:"40px", color:"white", margin:"0 3px"}}/>
          </div>
          <div style={{backgroundColor:"white", height:"200px", maxHeight:"200px", width:"100%"}}>
        </div>

      </div>
  </div>;
};
