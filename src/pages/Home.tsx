import React from 'react';
import { HeroSection } from '../components/HeroSection';
import { NavBar } from '../components/NavBar';
import { SectionOne } from '../components/SectionOne';

export const Home = () => {

  return <>
    <NavBar/>
    {/* <div style={{position:"relative"}}> */}

    <HeroSection/>
    {/* </div> */}
    <SectionOne/>
    <div style={{width:"100%", height:"100vh", backgroundColor:"green"}}>

    </div>
    <div style={{width:"100%", height:"100vh", backgroundColor:"pink"}}>

    </div>
  </>;
};
