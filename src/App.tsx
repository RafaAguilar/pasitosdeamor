import React from 'react';
import logo from './logo.svg';
import './App.css';
import { HeroSection } from './components/HeroSection';
import { Header } from './components/Header';
import { Home } from './pages/Home';

function App() {
  return (
   <>
    <Home/>
   </>
  );
}

export default App;
